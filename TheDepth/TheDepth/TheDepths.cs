﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using TheDepth.Input;
using TheDepth.States;
#endregion

namespace TheDepth
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TheDepths : Game
    {
        GraphicsDeviceManager graphics;
        ExtendedSpriteBatch spriteBatch;
        InputManager input;
        StateManager states;
        RenderTarget2D mainTarget;
        

        public StateManager States
        {
            get { return states; }
        }

        static TheDepths instance;

        public static TheDepths Instance
        {
            get
            {
                return instance;
            }
        }

        public TheDepths()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            instance = this;
            
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {

            mainTarget = new RenderTarget2D(graphics.GraphicsDevice, 320, 180);

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new ExtendedSpriteBatch(GraphicsDevice);
            states = new StateManager(this);
            input = new InputManager(this);

            Services.AddService(typeof(ExtendedSpriteBatch), spriteBatch);
            Services.AddService(typeof(StateManager), states);
            Services.AddService(typeof(InputManager), input);

            states.Push(new MainMapState(this));
            // TODO: use this.Content to load your game content here
        }

        public T GetService<T>()
        {
            return (T)Services.GetService(typeof(T));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            input.Update(gameTime);

            if (input.IsNewDown(ActionKeys.Cancel))
                Exit();

            states.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.SetRenderTarget(mainTarget);
            GraphicsDevice.Clear(Color.Black);
            states.Draw(gameTime);
            GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, SamplerState.PointClamp, null, null);
            spriteBatch.Draw(mainTarget, Window.ClientBounds, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
