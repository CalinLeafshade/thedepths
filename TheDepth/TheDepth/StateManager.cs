﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheDepth.States;
using Microsoft.Xna.Framework;

namespace TheDepth
{
    public class StateManager : List<GameState>
    {

        TheDepths game;

        public StateManager(TheDepths game)
        {
            this.game = game;
        }

        public GameState Peek()
        {
            return this[0];
        }

        public GameState Pop()
        {
            var gs = this[0];
            this.RemoveAt(0);
            return gs;
        }

        public GameState Push(GameState gs)
        {
            this.Insert(0, gs);
            return gs;
        }

        public void Draw(GameTime gameTime)
        {
            bool top = true;
            foreach (var s in this)
            {
                s.Draw(top);
                top = false;
                if (s.Opaque)
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            bool top = true;
            foreach (var s in this)
            {
                s.Update(gameTime, top);
                top = false;
            }
        }


        
    }
}
