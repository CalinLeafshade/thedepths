﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using TheDepth.Mapping;

namespace TheDepth
{
    public class Camera
    {

        public Vector2 Position { get; set; }

        MapObject following;
        Vector2 target;

        public Camera(int x, int y)
        {
            Position = target = new Vector2(x, y);
        }

        public void Follow(MapObject mo)
        {
            following = mo;
        }

        public void MoveTo(Vector2 pos, bool snap = false)
        {
            target = pos;
            if (snap)
                Position = target;
        }

        public void Update(GameTime gameTime)
        {
            if (following != null)
            {
                MoveTo(following.Position);
            }
            Position = new Vector2((float)MathHelper.Lerp(Position.X, target.X, gameTime.ElapsedGameTime.Milliseconds / 1000f),
                (float)MathHelper.Lerp(Position.Y, target.Y, gameTime.ElapsedGameTime.Milliseconds / 1000f));
        }

        public Matrix GetMatrix()
        {
            var m = Matrix.CreateTranslation(Position.X,Position.Y,0);
            return m;
        }
    }
}
