﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using TheDepth.Components;

namespace TheDepth
{

    public abstract class GameObject
    {

        private List<GameObject> children = new List<GameObject>();
        private List<Component> components = new List<Component>();

        public List<GameObject> Children
        {
            get { return children; }
            set { children = value; }
        }

        public void Update(GameTime gameTime) {}
        
    }

    
}
