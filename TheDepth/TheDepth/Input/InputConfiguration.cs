﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace TheDepth.Input
{
    
    [Serializable]
    class KeyboardConfigurationItem
    {
        public ActionKeys Action { get; set; }
        public Keys Key { get; set; }

        public KeyboardConfigurationItem(ActionKeys action, Keys key)
        {
            this.Action = action;
            this.Key = key;
        }
    }
    
    [Serializable]
    public class InputConfiguration
    {
        List<KeyboardConfigurationItem> KeyboardConfiguration { get; set; }

        public InputConfiguration()
        {
            KeyboardConfiguration = new List<KeyboardConfigurationItem>();
            defaults();
        }

        private void defaults()
        {
            SetKey(ActionKeys.Cancel, Keys.Escape);
            SetKey(ActionKeys.Select, Keys.Enter);
            SetKey(ActionKeys.Left, Keys.Left);
            SetKey(ActionKeys.Right, Keys.Right);
            SetKey(ActionKeys.Up, Keys.Up);
            SetKey(ActionKeys.Down, Keys.Down);
        }

        public Keys? KeyFromAction(ActionKeys action)
        {
            foreach (var kc in KeyboardConfiguration)
            {
                if (kc.Action == action)
                {
                    return kc.Key;
                }
            }
            return null;
        }

        public void SetKey(ActionKeys action, Keys key)
        {
            foreach (var kci in KeyboardConfiguration)
            {
                if (kci.Action == action)
                {
                    kci.Key = key;
                    return;
                }
            }
            var kc = new KeyboardConfigurationItem(action, key);
            KeyboardConfiguration.Add(kc);
        }
    }
}
