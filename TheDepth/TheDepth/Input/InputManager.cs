﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TheDepth.Input
{

    public enum ActionKeys { Up, Down, Left, Right, Select, Cancel };

    public class InputManager
    {

        InputConfiguration config = new InputConfiguration();

        private KeyboardState lastKeyboardState;
        private KeyboardState currentKeyboardState;

        TheDepths game;

        public InputManager(TheDepths game)
        {
            this.game = game;
        }

        public void Update(GameTime gameTime)
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();
            
        }

        public bool IsDown(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key);
        }

        public bool IsNewDown(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key) && !lastKeyboardState.IsKeyDown(key);
        }

        public bool IsDown(ActionKeys action)
        {
            var k = config.KeyFromAction(action);
            if (k != null && IsDown((Keys)k))
            {
                return true;
            }
            return false;
        }

        public bool IsNewDown(ActionKeys action)
        {
            var k = config.KeyFromAction(action);
            if (k != null && IsNewDown((Keys)k))
            {
                return true;
            }
            return false;
        }
    }
}
