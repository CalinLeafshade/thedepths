﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TheDepth.Components
{
    public abstract class Component
    {

        public virtual void OnAdd(GameObject to) { }

        public virtual void Draw(ExtendedSpriteBatch sb) { }
        public virtual void Update(GameTime gameTime) { }
    }
}
