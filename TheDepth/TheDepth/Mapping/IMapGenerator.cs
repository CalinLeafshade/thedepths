﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheDepth.Mapping
{
    interface IMapGenerator
    {
        Map Generate();
    }
}
