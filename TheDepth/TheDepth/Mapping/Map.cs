﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TheDepth.Mapping
{

    public enum TileType { Ground };

    public class MapTile
    {

        public const int TILESIZE = 16;

        public int X;
        public int Y;
        public TileType Type;

        public MapTile(int x, int y, TileType type)
        {
            X = x;
            Y = y;
            Type = type;
        }

        public Rectangle GetBounds()
        {
            return new Rectangle(X * TILESIZE, Y * TILESIZE, TILESIZE, TILESIZE);
        }

    }

    public class Map: IEnumerable<MapTile>
    {

        public int Size { get; set; }

        MapTile[,] tiles;

        public MapTile[,] Tiles
        {
            get { return tiles; }
            set { tiles = value; }
        }

        public Map(int size)
        {
            Size = size;
            tiles = new MapTile[size, size];
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(ExtendedSpriteBatch sb)
        {
            sb.Begin();
            foreach (MapTile mt in this)
            {
                sb.FillRectangle(mt.GetBounds(), Color.White);
            }
            sb.End();
        }

        public IEnumerator<MapTile> GetEnumerator()
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    if (tiles[x, y] != null)
                    {
                        yield return tiles[x,y];
                    }
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
