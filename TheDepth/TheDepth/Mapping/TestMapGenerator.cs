﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheDepth.Mapping
{
    class TestMapGenerator : IMapGenerator
    {

        int seed;
        int size;

        public TestMapGenerator(int seed, int size)
        {
            this.seed = seed;
            this.size = size;
        }

        public Map Generate()
        {
            var m = new Map(size);
            var rand = new Random(seed);
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    int i = rand.Next(10);
                    if (i < 1)
                    {
                        m.Tiles[x, y] = new MapTile(x, y, TileType.Ground);
                    }
                }
            }
            return m;
        }
    }
}
