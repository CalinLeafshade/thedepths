﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheDepth.Mapping;

namespace TheDepth.States
{
    class MainMapState : GameState
    {

        public Map Map { get; set; }

        public MainMapState(TheDepths game) : base(game) 
        {

            var gen = new TestMapGenerator(2, 32);

            Map = gen.Generate();
        
        }

        public override bool Opaque
        {
            get
            {
                return true;
            }
        }

        public override void Draw(bool focussed)
        {
            var sb = Game.GetService<ExtendedSpriteBatch>();
            Map.Draw(sb);
            base.Draw(focussed);
        }

    }
}
