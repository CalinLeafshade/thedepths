﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TheDepth.States
{
    public abstract class GameState
    {

        TheDepths game;

        public TheDepths Game
        {
            get { return game; }
            set { game = value; }
        }

        public GameState(TheDepths game)
        {
            this.game = game;
        }

        public virtual void Draw(bool focussed) {  }
        public virtual void Update(GameTime gameTime, bool focussed) {}

        public virtual bool Opaque
        {
            get { return false; }
        }

    }
}
